# Написать функцию, принимающую два аргумента. Функция должна :
# - если оба аргумента относятся к числовым типам - вернуть их произведение,
# - если к строкам - соединить в одну строку и вернуть,
# - если первый строка, а второй нет - вернуть словарь (dict), в котором ключ - первый аргумент, значение - второй
# в любом другом случае вернуть кортеж (tuple) из аргументов
def my_function(arg1, arg2):
    if type(arg1) == int or type(arg1) == float and type(arg2) == int or type(arg2) == float:
        res = arg1 + arg2
        return  res
    elif type(arg1) == str and type(arg2) == str:
        res = arg1 + arg2
        return res
    elif type(arg1) == str and type(arg2) != str:
        res = {}
        res[arg1] = arg2
        return res
    else:
        res = (arg1,arg2)
        return res
print((my_function(2,20.5)),'1<-----')
print(my_function('asd','asdsd'),'2<----')
print(my_function('key', 2),'3<---------')
print(my_function([23,23],'b23'),'4<------')


#Пишем игру ) Программа выбирает из диапазона чисел (1-100) случайное число и предлагает пользователю его угадать. Пользователь вводит число. Если #пользователь не угадал - предлагает пользователю угадать еще раз, пока он не угадает. Если угадал - спрашивает хочет ли он повторить игру (Y/N). #Если Y - повторить игру. N - закончить

import random

def my_game(arg1):
    x = random.randint( 0, 100 )
    print(x)
    if x == (arg1):
        res = True

    elif x != (arg1):
        res = False
    return res

answer = 'Y'
while answer == 'Y':
    number = my_game(int(input( "Введите номер: " )))
    if number == True:
        print('Совпало! Вы Выиграли миллион!')
        answer = input("Хотите ещё? Y/N : ")
    elif number == False:
        print("Неудача, попробуйте  ещё раз")







# #Пользователь вводит строку произвольной длины. Написать функцию, которая должна вернуть словарь следующего содержания:
# ключ - количество букв в слове, значение - список слов с таким количеством букв.
# отдельным ключем, например "0", записать количество пробелов.
# отдельным ключем, например "punctuation", записать все уникальные знаки препинания, которые есть в тексте.
# Например:

# "0": количество пробелов в строке
# "1": list слов из одной буквы
# "2": list слов из двух букв
# "3": list слов из трех букв
# и т.д ...
#
# "punctuation" : tuple уникальных знаков препинания

from pprint import pprint
def my_funktion(arg1):
    symbols = '/.,;";:]}[{)(*!@#$%^&'
    punctuation_list = []
    res = {0:arg1.count(' '),
           'punctuation':[]}
    for word in arg1.split():
        if word in symbols:
            res['punctuation'].append(word)
        elif len(word) not in res.keys():
            res[len(word)] = [word]
        elif len(word) in res.keys():
            res[len(word)].append(word)
    return res




pprint(my_funktion('1 22 333 4444 55555 1 22 333 4444 55555 666666'))
user_input = input('Введите строку : ')
pprint(my_funktion(user_input))


