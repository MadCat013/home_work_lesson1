# # Есть строка произвольного содержания. Написать код, который найдет в строке самое короткое слово, в котором
# # присутствуют подряд две гласные буквы.
my_string = input('Введите строку: ').split()
vowels = 'aeiouy'
my_words_list = []
for i in my_string:
    if len(i) < 2 and i in vowels or i[0] in vowels and i[1] in vowels:
        my_string2.append(i)
        word = min((my_string2), key=len)
if my_words_list == []:
    print("Слово в котором присутствуют подряд две гласные буквы не обнаружено")
else:
    print(word)


# Есть два числа - минимальная цена и максимальная цена. Дан словарь продавцов и цен на какой то товар у разных продавцов:
# { "citrus": 47.999, "istudio" 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324,
# "g-store": 37.166, "ipartner": 38.988, "sota": 37.720, "rozetka": 38.003}.
# Написать код, который найдет и выведет на экран список продавцов, чьи цены попадают в диапазон между нижней и верхней ценой.
# Например: lower_limit = 35.9  upper_limit = 37.3 ----->>> match: "g-store", "royal-service"
prise_dict = {"citrus": 47.999, "istudio": 42.999, "moyo": 49.999, "royal-service": 37.245, "buy.ua": 38.324,
               "g-store": 37.166, "ipartner": 38.988, "sota": 37.720, "rozetka": 38.003}
filtered_price = {}
while True:
    try:
        max_prise = float(input( "Введите максимальеую цену: " ))
        min_prise = float(input( "Введите минимальную цену: " ))
        break
    except ValueError:
        print("Введите цену ЧИСЛОМ!!!")
for key , value in prise_dict.items():
    if  value < max_prise and value > min_prise:
        filtered_price[key] = value
print("----->>> match:" + str(filtered_price.keys()))




