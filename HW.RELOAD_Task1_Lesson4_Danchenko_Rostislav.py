# Есть строка произвольного содержания. Написать код, который найдет в строке самое короткое слово,
# в котором присутствуют подряд две гласные буквы.
my_sentence = input('Введите строку: ').split()
vowels = "aeiouAEIOU"
my_words_list = []
for word in my_sentence:
    for char in word :
        if char in vowels :
            first_match = word.find(char)
            if  word in vowels:
                continue
            elif word[0] in vowels and word[1] in vowels:
                my_words_list.append( word )
            elif word.endswith(tuple(vowels))  and word[len(word)-2] in vowels:
                my_words_list.append( word )
            elif word.endswith(tuple(vowels)) and word[len(word)-2] not in vowels:
                continue
            elif word[first_match] in vowels and word[first_match-1] in vowels:
                my_words_list.append(word)
            elif word[first_match] in vowels and word[first_match-1] not in vowels:
                continue
            elif word[first_match] in vowels and word[first_match+1] in vowels:
                my_words_list.append( word )
if my_words_list == []:
    print("Слово в котором присутствуют подряд две гласные буквы не обнаружено")
else:
    word = min( (my_words_list), key=len )
    print("Слово --->",word)
