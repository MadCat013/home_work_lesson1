#Сформировать строку, в которой содержится информация определенном слове в строке.
# Например "The [номер] symbol in [тут слово] is [значение символа по номеру в слове]".
# Слово и номер получите с помощью input() или воспользуйтесь константой. Например (если слово - "Python" а символ 3)
# - "The 3 symbol in "Python" is t".

word = input('Введите слово: ')
while True:
    try:
        symbol_number = int( input( "Введите номер буквы: " ) )
        break
    except ValueError:
        print("Введите номер символа ЧИСЛОМ!!!")
symbol = "Can't be found!"
try:
    symbol = (word[symbol_number - 1])
except IndexError:
    print( 'String index out of range!' )

print(f"The '{symbol_number}' symbol in '{word}' is '{symbol}' ")



# Ввести из консоли строку. Определить количество слов в этой строке, которые заканчиваются на букву "o"
# (учтите, что буквы бывают заглавными).

my_str_lst = input('Введите строку: ').split()
counter = 0
for i in my_str_lst:
    if i.lower().endswith('o'):
        counter += 1
print('Количество слов в строке с окончанием "о" : ' + str(counter))



# Есть list с данными lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишите механизм, который формирует новый list (например lst2),
# который содержит только переменные-строки, которые есть в lst1.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []
for i in lst1:
    if type(i) == str:
        lst2.append(i)
print(lst2)
